# OLX Homework - by Pablo Pallocchi #

### ¿Cómo levantar el microservicio? ###

```
java -jar bin/olx-1.0.0-SNAPSHOT.jar server olx.yml
```

### ¿En qué puerto se exponen los servicios? ###

Los servicios se exponen por defecto en el puerto **9000**.

### ¿Cómo obtener la cuenta de prueba? ###

```
curl -XGET http://localhost:9000/api/account
```

### ¿Cómo registrar transacciones? ###

```
curl -XPOST -H "Content-Type: application/json" -d '{"amount":100,"sourceAccount":{"id":"1","bank":"Bank 1","country":"AR","balance":"100"},"destinationAccount":{"id":"2","bank":"Bank 2","country":"AR","balance":"100"}}' http://localhost:9000/api/transaction
```

### ¿Dónde se registran las transacciones? ###

Las transacciones se registran por defecto en **archivo.txt**, en el directorio desde donde se corrió la aplicación.

### ¿Cómo monitorear en tiempo real las transacciones? ###

```
tail -f archivo.txt
```

### ¿Cómo verificar el estado del microservicio? ###

```
curl -XGET http://localhost:9001/healthcheck
```

### ¿Cómo cambiar los parámetros configurables? ###

```
vi olx.yml
```