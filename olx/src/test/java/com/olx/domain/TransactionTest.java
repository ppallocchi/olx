/*******************************************************************************
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Pablo Pallocchi
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 *******************************************************************************/
package com.olx.domain;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

public class TransactionTest {
	
	private static final BigDecimal TRANSACTION_AMOUNT = new BigDecimal(100);
	
	private static final String BANK_1 = "Bank 1";
	private static final String BANK_2 = "Bank 2";
	
	@Test
	public void calculateLocalTaxWithSameBank() {
		Account source = createAccount(Country.AR, BANK_1);
		Account destination = createAccount(Country.AR, BANK_1);
		Transaction transaction = createLocalTransaction(source, destination);
		assertEquals(BigDecimal.ZERO, transaction.calculateTax());
	}
	
	@Test
	public void calculateLocalTaxWithDifferentBanks() {
		Account source = createAccount(Country.AR, BANK_1);
		Account destination = createAccount(Country.AR, BANK_2);
		Transaction transaction = createLocalTransaction(source, destination);
		assertEquals(Tax.LOCAL.getPercentage(), transaction.calculateTax());
	}
	
	@Test
	public void calculateInternationalTax() {
		Account source = createAccount(Country.AR, BANK_1);
		Account destination = createAccount(Country.US, BANK_1);
		Transaction transaction = createInternationalTransaction(source, destination);
		assertEquals(Tax.INTERNATIONAL.getPercentage(), transaction.calculateTax());
	}
	
	public static Account createAccount(Country country, String bank) {
		Account account = new Account();
		account.setBank(bank);
		return account;
	}
	
	public static Transaction createLocalTransaction(Account source, Account destination) {
		Transaction transaction = new LocalTransaction();
		transaction.setAmount(TRANSACTION_AMOUNT);
		transaction.setDestinationAccount(source);
		transaction.setSourceAccount(destination);
		return transaction;
	}
	
	public static Transaction createInternationalTransaction(Account source, Account destination) {
		Transaction transaction = new InternationalTransaction();
		transaction.setAmount(TRANSACTION_AMOUNT);
		transaction.setDestinationAccount(source);
		transaction.setSourceAccount(destination);
		return transaction;
	}

}