/*******************************************************************************
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Pablo Pallocchi
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 *******************************************************************************/
package com.olx.repositories.impl;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.olx.OlxConfiguration;
import com.olx.domain.Transaction;
import com.olx.repositories.TransactionRepository;

import io.dropwizard.lifecycle.Managed;

public class TxtTransactionRepositoryImpl implements TransactionRepository, Managed {
	
	private static final Logger logger = LoggerFactory.getLogger(TxtTransactionRepositoryImpl.class);
	
	private static final String FORMAT = "{0} id:{1} source:{2} destination:{3} amount:{4} status:{5}";
	
	@Inject
	OlxConfiguration conf;
	
	private static AtomicLong idCounter = new AtomicLong();
	
	private BufferedWriter buffer;
	
	private SimpleDateFormat dateFormatter;
	
	@Inject
	public void init() {
		dateFormatter = new SimpleDateFormat(conf.getTransactions().getDatetimeFormat());
	}
	
	@Override
	public void start() throws Exception {
		logger.info("Open '{}' file for transaction registration", conf.getTransactions().getOutput());
		buffer = new BufferedWriter(new FileWriter(conf.getTransactions().getOutput()));
	}
	
    @Override
    public void stop() throws Exception {
    	logger.info("Close '{}' file", conf.getTransactions().getOutput());
    	buffer.close();
    }

	@Override
	public void save(Transaction transaction) {
		assignIdWhenNull(transaction);
		String line = format(transaction);
		try {
			write(line);
		} catch (IOException e) {
			logger.error("Transaction cannot be saved in txt repository: " + line, e);
		}
	}
	
	protected void assignIdWhenNull(Transaction transaction) {
		if(transaction.getId() == null)
			transaction.setId(idCounter.getAndIncrement());
	}
	
	protected String format(Transaction transaction) {
		return MessageFormat.format(FORMAT,
				dateFormatter.format(new Date()),
				transaction.getId(),
				transaction.getSourceAccount().getId(), 
				transaction.getDestinationAccount().getId(), 
				transaction.getAmount(), 
				transaction.getStatus());
	}
	
    public synchronized void write(String line) throws IOException {
    	buffer.write(line);
    	buffer.newLine();
    	buffer.flush();
    }

}