/*******************************************************************************
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Pablo Pallocchi
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 *******************************************************************************/
package com.olx.health;

import com.codahale.metrics.health.HealthCheck;
import com.google.inject.Inject;
import com.olx.domain.Account;
import com.olx.repositories.AccountRepository;

public class AccountHealthCheck extends HealthCheck {
	
	@Inject
	AccountRepository accountRepository;

	@Override
	protected Result check() throws Exception {
		Account account = accountRepository.findTestAccount();
		if(account != null)
			return Result.healthy();
		return Result.unhealthy("Cannot retrieve test account");
	}

}