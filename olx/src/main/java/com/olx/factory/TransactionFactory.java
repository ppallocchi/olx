package com.olx.factory;

import com.olx.domain.Account;
import com.olx.domain.InternationalTransaction;
import com.olx.domain.LocalTransaction;
import com.olx.domain.Transaction;

public class TransactionFactory {
	
	public static Transaction getTransaction(Account source, Account destination) {
		if(source.getCountry().equals(destination.getCountry())) {
			return new LocalTransaction();
		}
		return new InternationalTransaction();
	}

}