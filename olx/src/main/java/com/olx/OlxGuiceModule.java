/*******************************************************************************
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Pablo Pallocchi
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 *******************************************************************************/
package com.olx;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.olx.repositories.AccountRepository;
import com.olx.repositories.TransactionRepository;
import com.olx.repositories.impl.TxtTransactionRepositoryImpl;
import com.olx.repositories.impl.YmlAccountRepositoryImpl;
import com.olx.services.TransactionService;
import com.olx.services.impl.TransactionServiceImpl;

public class OlxGuiceModule extends AbstractModule {
	
	private OlxConfiguration conf;
	
	public OlxGuiceModule(OlxConfiguration conf) {
		this.conf = conf;
	}

	@Override
	protected void configure() {
		bind(OlxConfiguration.class).toInstance(conf);
		bind(AccountRepository.class).to(YmlAccountRepositoryImpl.class);
		bind(TransactionRepository.class).to(TxtTransactionRepositoryImpl.class);
		bind(TxtTransactionRepositoryImpl.class).in(Scopes.SINGLETON);
		bind(TransactionService.class).to(TransactionServiceImpl.class);
		bind(TransactionServiceImpl.class).in(Scopes.SINGLETON);
	}

}