/*******************************************************************************
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Pablo Pallocchi
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 *******************************************************************************/
package com.olx.utils;

import java.math.BigDecimal;

public class Accounting {
	
	public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
	
	public static BigDecimal percentage(BigDecimal amount, BigDecimal perecentage){
	    return amount.multiply(perecentage).divide(ONE_HUNDRED);
	}

}