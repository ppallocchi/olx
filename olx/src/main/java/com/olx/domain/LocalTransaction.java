/*******************************************************************************
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Pablo Pallocchi
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 *******************************************************************************/
package com.olx.domain;

import java.math.BigDecimal;

import com.olx.utils.Accounting;

public class LocalTransaction extends Transaction {

	@Override
	public BigDecimal calculateTax() {
		BigDecimal tax = BigDecimal.ZERO;
		if(!getSourceAccount().getBank().equals(getDestinationAccount().getBank()))
			tax = Accounting.percentage(getAmount(), Tax.LOCAL.getPercentage());
		return tax;
	}	

}