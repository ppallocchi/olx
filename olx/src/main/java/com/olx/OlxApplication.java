/*******************************************************************************
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Pablo Pallocchi
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 *******************************************************************************/
package com.olx;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.olx.domain.InternationalTransaction;
import com.olx.domain.LocalTransaction;
import com.olx.domain.Transaction;
import com.olx.health.AccountHealthCheck;
import com.olx.repositories.impl.TxtTransactionRepositoryImpl;
import com.olx.resources.AccountResource;
import com.olx.resources.TransactionResource;
import com.olx.serializer.Mixin;
import com.olx.services.impl.TransactionServiceImpl;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

public class OlxApplication extends Application<OlxConfiguration> {
	
	public static void main(String[] args) throws Exception {
		new OlxApplication().run(args);
	}

    @Override
    public void run(OlxConfiguration configuration, Environment environment) {
    	Injector injector = Guice.createInjector(new OlxGuiceModule(configuration));
    	initSerializer(environment.getObjectMapper());
    	initHelathChecks(environment, injector);
    	initResources(environment, injector);
    	initManaged(environment, injector);
    }
    
    protected void initSerializer(ObjectMapper om) {
    	om.addMixIn(Transaction.class, Mixin.Transaction.class);
    	om.addMixIn(LocalTransaction.class, Mixin.LocalTransaction.class);
    	om.addMixIn(InternationalTransaction.class, Mixin.InternationalTransaction.class);
    }
    
    protected void initHelathChecks(Environment environment, Injector injector) {
    	environment.healthChecks().register("accounts", injector.getInstance(AccountHealthCheck.class));
    }
    
    protected void initResources(Environment environment, Injector injector) {
    	environment.jersey().register(injector.getInstance(AccountResource.class));
    	environment.jersey().register(injector.getInstance(TransactionResource.class));
    }
    
    protected void initManaged(Environment environment, Injector injector) {
    	environment.lifecycle().manage(injector.getInstance(TxtTransactionRepositoryImpl.class));
    	environment.lifecycle().manage(injector.getInstance(TransactionServiceImpl.class));
    }
    
	@Override
	public String getName() {
		return "olx";
	}
	
}