package com.olx.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.olx.domain.Account;
import com.olx.domain.Transaction;
import com.olx.factory.TransactionFactory;

public class TransactionDeserializer extends JsonDeserializer<Transaction> {
	
	private static final String SOURCE_ACCOUNT = "sourceAccount";
	private static final String DESTINATION_ACCOUNT = "destinationAccount";

	@Override
	public Transaction deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectMapper mapper = (ObjectMapper) parser.getCodec();
        ObjectNode root = mapper.readTree(parser);
        if(root.has(SOURCE_ACCOUNT) && root.has(DESTINATION_ACCOUNT)) {
        	Account source = mapper.readValue(root.get(SOURCE_ACCOUNT).toString(), Account.class);
        	Account destination = mapper.readValue(root.get(DESTINATION_ACCOUNT).toString(), Account.class);
        	return mapper.readValue(root.toString(), TransactionFactory.getTransaction(source, destination).getClass());
        }
        throw ctxt.mappingException("Transaction must contain source and destination accounts!");
	}

}