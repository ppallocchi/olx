package com.olx.serializer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public interface Mixin {
	
	@JsonDeserialize(using = TransactionDeserializer.class)
	public static interface Transaction {};
	
	@JsonDeserialize(as = com.olx.domain.LocalTransaction.class)
	public static interface LocalTransaction {};
	
	@JsonDeserialize(as = com.olx.domain.InternationalTransaction.class)
	public static interface InternationalTransaction {};

}