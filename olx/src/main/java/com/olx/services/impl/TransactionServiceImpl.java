/*******************************************************************************
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Pablo Pallocchi
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 *******************************************************************************/
package com.olx.services.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.olx.OlxConfiguration;
import com.olx.domain.Transaction;
import com.olx.domain.TransactionStatus;
import com.olx.repositories.TransactionRepository;
import com.olx.services.TransactionService;

import io.dropwizard.lifecycle.Managed;

public class TransactionServiceImpl implements TransactionService, Managed {
	
	private static final Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);
	
	@Inject
	OlxConfiguration conf;
	
	@Inject
	TransactionRepository transactionRepository;
	
	ExecutorService executor;
	
	@Override
	public void start() throws Exception {
		logger.info("Initialize thread pool with {} threads ", conf.getTransactions().getThreadPoolSize());
		executor = Executors.newFixedThreadPool(conf.getTransactions().getThreadPoolSize());
	}
	
	@Override
	public void stop() throws Exception {
		logger.info("Shutdown thread pool and wait for termination");
		executor.shutdown();
		executor.awaitTermination(5, TimeUnit.SECONDS);
	}
	
	@Override
	public void register(Transaction transaction) {
		
		transaction.setStatus(TransactionStatus.PENDING);
		transactionRepository.save(transaction);
		
		executor.execute(() -> {
		
			transaction.setStatus(TransactionStatus.IN_PROGRESS);
			transactionRepository.save(transaction);
			
			process(transaction);
			
			transaction.setStatus(TransactionStatus.COMPLETE);
			transactionRepository.save(transaction);
		});
	}
	
	protected void process(Transaction transaction) {
		try {
			logger.info("Processing transaction...");
			Thread.sleep(2000);
			logger.info("Transaction OK.");
		} catch (InterruptedException e) {
			logger.warn("Cannot emulate transaction processing");
		}
	}

}