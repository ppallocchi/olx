/*******************************************************************************
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Pablo Pallocchi
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 *******************************************************************************/
package com.olx;

import javax.validation.constraints.NotNull;

import com.olx.domain.Account;

import io.dropwizard.Configuration;

public class OlxConfiguration extends Configuration {
	
	@NotNull
	private Account account;
	
	@NotNull
	private TransactionsConfiguration transactionsConfiguration;
	
	public Account getAccount() {
		return account;
	}
	
	public void setAccount(Account account) {
		this.account = account;
	}
	
	public TransactionsConfiguration getTransactions() {
		return transactionsConfiguration;
	}
	
	public void setTransactions(TransactionsConfiguration transactionsConfiguration) {
		this.transactionsConfiguration = transactionsConfiguration;
	}
	
	public static class TransactionsConfiguration {
		
		@NotNull
		private String datetimeFormat;
		
		@NotNull
		private int threadPoolSize;
		
		@NotNull
		private String output;
		
		public String getDatetimeFormat() {
			return datetimeFormat;
		}
		
		public void setDatetimeFormat(String datetimeFormat) {
			this.datetimeFormat = datetimeFormat;
		}
		
		public int getThreadPoolSize() {
			return threadPoolSize;
		}
		
		public void setThreadPoolSize(int threadPoolSize) {
			this.threadPoolSize = threadPoolSize;
		}
		
		public String getOutput() {
			return output;
		}
		
		public void setOutput(String output) {
			this.output = output;
		}

	}
	
}